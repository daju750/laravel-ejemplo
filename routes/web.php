<?php

Route::get('/','PagesController@inicio')->name('inicio');

Route::get('/detalle/{id}','PagesController@detalle')->name('notas.detalle');

Route::post('/nota/guardar','PagesController@crear')->name('notas.crear');