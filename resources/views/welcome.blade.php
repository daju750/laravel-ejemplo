@extends('plantilla')

  @section('seccion')

    <h1 class="display-4">Notas</h1>
    
    @if(session('mensaje'))
    <div class="alert alert-success">
        {{ session('mensaje') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif


    <form action="{{route('notas.crear')}}" method="POST">{{csrf_field()}}
      <!-- <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"> -->
    
        @if($errors->has('nombre'))
        <div class="alert alert-danger">El nombre es obligatorio
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
      
      <input type="text" name="nombre" placeholder="nombre" class="form-control mb-2" value="{{ old('nombre') }}">

        @if($errors->has('descripcion'))
        <div class="alert alert-danger">El descripcion es obligatorio
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

      <input type="text" name="descripcion" placeholder="descripcion" class="form-control mb-2" value="{{ old('descripcion') }}">

      <button class="btn btn-primary btn-block" type="submit">Agregar</button>
    </form>
    <hr class="hr" />
        <table class="table">
        <thead>
            <tr>
            <th scope="col">#id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Descripcion</th>
            <th scope="col">Handle</th>
            </tr>
        </thead>
        <tbody>
        @foreach($notas as $item)
            <tr>
                <th scope="row">{{$item->id}}</th>
                <th>
                    <a href="{{route('notas.detalle', $item)}}">
                        {{$item->nombre}}
                    </a>
                </th>
                <th>{{$item->descripcion}}</th>
                <th>@mdo</th>
            </tr>
        @endforeach()
        </tbody>
        </table>

  @endsection