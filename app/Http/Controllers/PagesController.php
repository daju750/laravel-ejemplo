<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nota;

class PagesController extends Controller{
    
    public function inicio(){
        $notas = Nota::all();
        return view('welcome',compact('notas'));
    }

    public function detalle($id) {
        $nota = Nota::findOrFail($id);
        return view('notas.detalle',compact('nota'));
    }

    public function crear(Request $request){

        $this->validate($request,[
            'nombre'              => 'required',
            'descripcion'             => 'required',
        ]);

        $nueva_nota = new Nota;
        $nueva_nota->nombre = $request->nombre;
        $nueva_nota->descripcion = $request->descripcion;
        $nueva_nota->save();
        return redirect()->route('inicio')->with('mensaje','Nota Agregada');
    }

}
